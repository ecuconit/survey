﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions; // Regular expression
using System.Net;
using System.Configuration;
using System.Net.Mail; // Email class

namespace CON_Console
{
	/// <summary>
	/// Generic database connection object for DB check
	/// </summary>
	internal static class DatabaseController
	{
		#region Private Method
		/// <summary>
		/// Get default connection string
		/// </summary>
		internal static string connectionString
		{
			get
			{
				return ConfigurationManager.ConnectionStrings["database"].ConnectionString;
			}
		}
		#endregion

		#region ECU Account Validation
		/// <summary>
		/// Contact CON's ECU verification server to see if a secured file can be fetched
		/// </summary>
		/// <param name="userName">ECU user name</param>
		/// <param name="password">ECU intra password</param>
		/// <param name="errorMessage">Error message, if any</param>
		/// <returns>Whether the account is okay or not</returns>
		public static bool ECUVerification(string userName, string password, out string errorMessage)
		{
			// Simple verification 
			if ((userName == null) || (userName.Trim().Equals(string.Empty)) || (password == null) || (password.Trim().Equals(string.Empty)))
			{
				errorMessage = "[Parameter Verification Error]: Invalid parameter [User Name] or [Password] to perform [ECU Verification] operation.";
				return false;
			}

			// Initialize holder
			errorMessage = null;

			// Building the user's credential into an object used for connection
			string remoteUrl = ConfigurationManager.AppSettings["AccountVerifyDir"];
			string fileName = ConfigurationManager.AppSettings["AccountVerifyFile"];
			WebClient client = new WebClient();
			string content = string.Empty;
			try
			{
				NetworkCredential credential = new NetworkCredential(userName, password);
				client.Credentials = credential;
				Stream data = client.OpenRead(remoteUrl + fileName);
				StreamReader reader = new StreamReader(data);
				content = reader.ReadToEnd();
				reader.Close();
			}
			catch (Exception error)
			{
				errorMessage = "Account verification server connection error.  Error message: " + error.Message;
				return false;
			}

			// Check for reply
			if (content == "OK")
				return true;
			errorMessage = "Invalid ECU user name and/or password.";
			return false;
		}

		/// <summary>
		/// Whether the user email account is an ECU email or not
		/// </summary>
		/// <param name="userName">User's email account</param>
		/// <returns>True if email has ECU ending</returns>
		public static bool IsECUAccount(string userName)
		{
			string emailEnding = userName.Substring(userName.IndexOf('@') + 1, userName.Length - userName.IndexOf('@') - 1);
			foreach (string ending in ConfigurationManager.AppSettings["ECUMailEndings"].Split(','))
			{
				if (ending.Trim().ToUpper().Equals(emailEnding.ToUpper()))
					return true;
			}
			return false;
		}
		#endregion

		#region Public Develop Database Interface Methods
		/// <summary>
		/// Call the database procedure to use a select stored procedure
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <returns>Return object on whatever the database returns</returns>
		public static DataTable SelectReturn(string procedureName, Dictionary<string, object> parameters, ref string errorMessage)
		{
			return SelectReturn(procedureName, parameters, connectionString, ref errorMessage);
		}

		/// <summary>
		/// Call the database procedure to use a select stored procedure
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <returns>Return object on whatever the database returns</returns>
		public static DataSet SelectCollectionReturn(string procedureName, Dictionary<string, object> parameters, ref string errorMessage)
		{
			return SelectCollectionReturn(procedureName, parameters, connectionString, ref errorMessage);
		}

		/// <summary>
		/// Call the procedure to insert a new record into the database with a return parameter
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <returns>Return message from the database</returns>
		public static object InsertUpdateReturn(string procedureName, Dictionary<string, object> parameters, ref string errorMessage)
		{
			return InsertUpdateReturn(procedureName, parameters, connectionString, ref errorMessage);
		}

		/// <summary>
		/// Call the database procedure to use a select stored procedure
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <param name="preProcedureName">Which procedure to call before the operation</param>
		/// <param name="postProcedureName">Which procedure to call after the operation</param>
		/// <returns>Return object on whatever the database returns</returns>
		public static DataSet SelectBatchReturn(string procedureName, List<Dictionary<string, object>> parameters, ref string errorMessage, string preProcedureName = null, string postProcedureName = null)
		{
			return SelectBatchReturn(procedureName, parameters, connectionString, ref errorMessage);
		}

		/// <summary>
		/// Call the procedure to insert a new record into the database with a return parameter
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <param name="preProcedureName">Which procedure to call before the operation</param>
		/// <param name="postProcedureName">Which procedure to call after the operation</param>
		/// <returns>Return message from the database</returns>
		public static List<object> InsertUpdateBatchReturn(string procedureName, List<Dictionary<string, object>> parameters, ref string errorMessage, string preProcedureName = null, string postProcedureName = null)
		{
			return InsertUpdateBatchReturn(procedureName, parameters, connectionString, ref errorMessage);
		}
		#endregion

		#region Private Database Interface Methods
		/// <summary>
		/// Call the database procedure to use a select stored procedure
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <param name="connection">Connection string of which database to connect to</param>
		/// <returns>Return object on whatever the database returns</returns>
		private static DataTable SelectReturn(string procedureName, Dictionary<string, object> parameters, string connection, ref string errorMessage)
		{
			DataTable result = new DataTable();
			errorMessage = null;

			// Using the block to ensure disposing of connections
			using (SqlConnection conn = new SqlConnection(connection))
			{
				// Establish data connection objects
				SqlCommand comm = new SqlCommand(procedureName, conn);
				SqlDataAdapter adapt = new SqlDataAdapter(comm);

				try
				{
					// Adding parameters to command
					comm.CommandType = CommandType.StoredProcedure;
					if (parameters != null)
						foreach (string key in parameters.Keys)
							comm.Parameters.Add(new SqlParameter(key, parameters[key] ?? DBNull.Value));

					// Establish connection and fetch data
					conn.Open();
					adapt.Fill(result);
				}
				catch (Exception error)
				{
					errorMessage = error.Message + ". Stack: " + error.StackTrace;
				}
				finally
				{
					conn.Close();
				}
			}

			// Return the resulting table
			return result;
		}

		/// <summary>
		/// Call the database procedure to use a select stored procedure
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <param name="connection">Connection string of which database to connect to</param>
		/// <returns>Return a collection of datatables for mass query</returns>
		private static DataSet SelectCollectionReturn(string procedureName, Dictionary<string, object> parameters, string connection, ref string errorMessage)
		{
			DataSet result = new DataSet();
			errorMessage = null;

			// Using the block to ensure disposing of connections
			using (SqlConnection conn = new SqlConnection(connection))
			{
				// Establish data connection objects
				SqlCommand comm = new SqlCommand(procedureName, conn);
				SqlDataAdapter adapt = new SqlDataAdapter(comm);

				try
				{
					// Adding parameters to command
					comm.CommandType = CommandType.StoredProcedure;
					if (parameters != null)
						foreach (string key in parameters.Keys)
							comm.Parameters.Add(new SqlParameter(key, parameters[key] ?? DBNull.Value));

					// Establish connection and fetch data
					conn.Open();
					adapt.Fill(result);
				}
				catch (Exception error)
				{
					errorMessage = error.Message + ". Stack: " + error.StackTrace;
				}
				finally
				{
					conn.Close();
				}
			}

			// Return the resulting table
			return result;
		}

		/// <summary>
		/// Call the procedure to insert a new record into the database with a return parameter
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <param name="connection">Connection string of which database to connect to</param>
		/// <returns>Return message from the database</returns>
		private static object InsertUpdateReturn(string procedureName, Dictionary<string, object> parameters, string connection, ref string errorMessage)
		{
			object result = null;
			// Using the block to ensure disposing of connections
			using (SqlConnection conn = new SqlConnection(connection))
			{
				// Establish data connection objects
				SqlCommand comm = new SqlCommand(procedureName, conn);

				try
				{
					// Adding parameters to command
					comm.CommandType = CommandType.StoredProcedure;
					if (parameters != null)
						foreach (string key in parameters.Keys)
							comm.Parameters.Add(new SqlParameter(key, parameters[key] ?? DBNull.Value));
					comm.Parameters.Add(new SqlParameter("storedProcedureReturnedValue", null));
					comm.Parameters["storedProcedureReturnedValue"].Direction = ParameterDirection.ReturnValue;

					// Establish connection and fetch data
					conn.Open();
					comm.ExecuteNonQuery();

					result = comm.Parameters["storedProcedureReturnedValue"].Value;
				}
				catch (Exception error)
				{
					errorMessage = error.Message + ". Stack: " + error.StackTrace;
				}
				finally
				{
					conn.Close();
				}
			}

			// Return the resulting table
			return result;
		}

		/// <summary>
		/// Call the database procedure to use a select stored procedure in a batch transaction
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <param name="connection">Connection string of which database to connect to</param>
		/// <param name="preProcedureName">Which procedure to call before the operation</param>
		/// <param name="postProcedureName">Which procedure to call after the operation</param>
		/// <returns>A collection of datatables for each command in transaction</returns>
		private static DataSet SelectBatchReturn(string procedureName, List<Dictionary<string, object>> parameters, string connection, ref string errorMessage, string preProcedureName = null, string postProcedureName = null)
		{
			DataSet result = new DataSet();
			DataTable temp = new DataTable();
			SqlTransaction transaction = null;
			errorMessage = null;

			// Using the block to ensure disposing of connections
			using (SqlConnection conn = new SqlConnection(connection))
			{
				// Establish data connection objects
				SqlCommand comm = new SqlCommand(procedureName, conn);
				SqlDataAdapter adapt = new SqlDataAdapter(comm);

				try
				{
					// Open connection
					conn.Open();
					transaction = conn.BeginTransaction("Batch");
					comm.CommandType = CommandType.StoredProcedure;
					comm.Transaction = transaction;

					// If there is a pre-condition, call it
					if (!string.IsNullOrEmpty(preProcedureName))
					{
						comm.CommandText = preProcedureName;
						comm.Parameters.Clear();
						adapt.Fill(temp);
						result.Tables.Add(temp);
					}

					// Process each command
					comm.CommandText = procedureName;
					foreach (Dictionary<string, object> param in parameters)
					{
						// Adding parameters to command
						comm.Parameters.Clear();
						if (parameters != null)
							comm.Parameters.AddRange(param.Keys.Select(x => new SqlParameter(x, param[x] ?? DBNull.Value)).ToArray());

						// Establish connection and fetch data
						adapt.Fill(temp);
						result.Tables.Add(temp);
					}

					// If there is a post-condition, call it
					if (!string.IsNullOrEmpty(preProcedureName))
					{
						comm.CommandText = postProcedureName;
						comm.Parameters.Clear();
						adapt.Fill(temp);
						result.Tables.Add(temp);
					}

					// Commit to transaction
					transaction.Commit();
				}
				catch (Exception error)
				{
					errorMessage = error.Message + ". Stack: " + error.StackTrace;

					// Attempt to rollback
					try
					{
						transaction.Rollback();
					}
					catch (Exception rollbackError)
					{
						errorMessage += ". Rollback Exception Type: " + rollbackError.GetType() + ". Message: " + rollbackError.Message;
					}
				}
				finally
				{
					conn.Close();
				}
			}

			// Return the resulting table
			return result;
		}

		/// <summary>
		/// Call the procedure to insert a new record into the database with a return parameter in a batch transaction
		/// </summary>
		/// <param name="procedureName">What procedure to call</param>
		/// <param name="parameters">List of parameters for the procedure</param>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <param name="connection">Connection string of which database to connect to</param>
		/// <param name="preProcedureName">Which procedure to call before the operation</param>
		/// <param name="postProcedureName">Which procedure to call after the operation</param>
		/// <returns>Return message from the database</returns>
		private static List<object> InsertUpdateBatchReturn(string procedureName, List<Dictionary<string, object>> parameters, string connection, ref string errorMessage, string preProcedureName = null, string postProcedureName = null)
		{
			List<object> result = new List<object>();
			SqlTransaction transaction = null;
			errorMessage = null;

			// Using the block to ensure disposing of connections
			using (SqlConnection conn = new SqlConnection(connection))
			{
				// Establish data connection objects
				SqlCommand comm = new SqlCommand(procedureName, conn);
				SqlDataAdapter adapt = new SqlDataAdapter(comm);

				try
				{
					// Open connection
					conn.Open();
					transaction = conn.BeginTransaction("Batch");
					comm.CommandType = CommandType.StoredProcedure;
					comm.Transaction = transaction;

					// If there is a pre-condition, call it
					if (!string.IsNullOrEmpty(preProcedureName))
					{
						comm.CommandText = preProcedureName;
						comm.Parameters.Clear();
						comm.ExecuteNonQuery();
						result.Add(comm.Parameters["storedProcedureReturnedValue"].Value);
					}

					// Process each command
					comm.CommandText = procedureName;
					foreach (Dictionary<string, object> param in parameters)
					{
						// Adding parameters to command
						comm.Parameters.Clear();
						if (parameters != null)
							comm.Parameters.AddRange(param.Keys.Select(x => new SqlParameter(x, param[x] ?? DBNull.Value)).ToArray());
						comm.Parameters.Add(new SqlParameter("storedProcedureReturnedValue", null));
						comm.Parameters["storedProcedureReturnedValue"].Direction = ParameterDirection.ReturnValue;

						// Establish connection and fetch data
						comm.ExecuteNonQuery();
						result.Add(comm.Parameters["storedProcedureReturnedValue"].Value);
					}

					// If there is a post-condition, call it
					if (!string.IsNullOrEmpty(preProcedureName))
					{
						comm.CommandText = postProcedureName;
						comm.Parameters.Clear();
						comm.ExecuteNonQuery();
						result.Add(comm.Parameters["storedProcedureReturnedValue"].Value);
					}

					// Commit to transaction
					transaction.Commit();
				}
				catch (Exception error)
				{
					errorMessage = error.Message + ". Stack: " + error.StackTrace;

					// Attempt to rollback
					try
					{
						transaction.Rollback();
					}
					catch (Exception rollbackError)
					{
						errorMessage += ". Rollback Exception Type: " + rollbackError.GetType() + ". Message: " + rollbackError.Message;
					}
				}
				finally
				{
					conn.Close();
				}
			}

			// Return the resulting table
			return result;
		}
		#endregion

		#region Helper Method
		/// <summary>
		/// Check to see whether the provided email address is a valid expression
		/// </summary>
		/// <param name="strIn">Input email string</param>
		/// <returns>Whether the input is a valid email address</returns>
		public static bool IsValidEmail(string strIn)
		{
			// Return true if strIn is in valid e-mail format.
			return Regex.IsMatch(strIn, @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
			 @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");
		}
		#endregion

		#region Get Server Time
		/// <summary>
		/// Get the current time on the server
		/// </summary>
		/// <param name="errorMessage">Pass by reference of error message, null if no error</param>
		/// <returns>Current server time</returns>
		public static DateTime GetServerTime(ref string errorMessage)
		{
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				// Establish data connection objects
				SqlCommand comm = new SqlCommand("SELECT GETDATE ()", conn);
				SqlDataAdapter adapt = new SqlDataAdapter(comm);
				DataTable result = new DataTable();

				try
				{
					// Adding parameters to command
					comm.CommandType = CommandType.Text;

					// Establish connection and fetch data
					conn.Open();
					adapt.Fill(result);
					conn.Close();

					// Error checking
					if ((result.Rows.Count != 1) || (result.Columns.Count != 1))
					{
						errorMessage = "[Data Fetch Error]: Invalid number of rows or columns being returned.";
						return DateTime.Now;
					}
					return (DateTime)result.Rows[0][0];
				}
				catch (Exception error)
				{
					errorMessage = "[Database Error]: " + error.Message + ". Stack: " + error.StackTrace;
					return DateTime.Now;
				}
				finally
				{
					conn.Close();
				}
			}
		}
		#endregion

		#region Emailer
		/// <summary>
		/// Send a email, use default title line
		/// </summary>
		/// <param name="sendTo">Who to send to</param>
		/// <param name="copyTo">Who to copy the mail to</param>
		/// <param name="message">Body of the email</param>
		/// <param name="errorMessage">Error message, if any</param>
		/// <returns>Whether or not the operation was completed successfully</returns>
		public static bool SendMail(List<string> sendTo, List<string> copyTo, string message, ref string errorMessage)
		{
			return SendMail(sendTo, copyTo, null, message, ref errorMessage);
		}

		/// <summary>
		/// Send a email
		/// </summary>
		/// <param name="sendTo">Who to send to</param>
		/// <param name="copyTo">Who to copy the mail to</param>
		/// <param name="title">Subject line of the message, null to use default</param>
		/// <param name="message">Body of the email</param>
		/// <param name="errorMessage">Error message, if any</param>
		/// <param name="from">Overwrite sender if necessary</param>
		/// <returns>Whether or not the operation was completed successfully</returns>
		public static bool SendMail(List<string> sendTo, List<string> copyTo, string title, string message, ref string errorMessage, string from = null)
		{
			// Buffer necessary data ^o^ problem, can't hit ECU server
			MailMessage mail;
			string server = ConfigurationManager.AppSettings["smtp"].ToString();
			string sender = from ?? ConfigurationManager.AppSettings["sender"].ToString();
			string subject = title ?? ConfigurationManager.AppSettings["subject"].ToString();
			string replyTo = ConfigurationManager.AppSettings["replyTo"].ToString();
			string password = ConfigurationManager.AppSettings["smtpPassword"].ToString();
			errorMessage = null;

			// Sending e-mail
			try
			{
				// Simple verification
				if ((sendTo == null) || (sendTo.Count <= 0))
				{
					errorMessage = "Unable to deliver mail, has no send-to field.";
					return false;
				}
				if (string.IsNullOrWhiteSpace(message))
				{
					errorMessage = "Unable to deliver mail, has no message.";
					return false;
				}

				// Build email object
				mail = new MailMessage();
				foreach (string to in sendTo)
					mail.To.Add(to);
				if (copyTo != null)
					foreach (string cc in copyTo)
						mail.CC.Add(cc);
				mail.From = new MailAddress(sender);
				mail.Subject = subject;
				mail.Body = string.Format("{0}\r\n\r\n{1}", message, "Please do not reply to this automatically generated e-mail.");
				mail.ReplyToList.Add(new MailAddress(replyTo));
				mail.IsBodyHtml = false;

				// Setup SMTP server and mail the object
				SmtpClient client = new SmtpClient();
				client.Host = server;

				// client.UseDefaultCredentials = false;
				// client.Credentials = new NetworkCredential(sender, password);
				client.Send(mail);
			}
			catch (Exception error)
			{
				errorMessage = "Failure occurred during emailing process. Error Message: " + (error.Message ?? "Unknown Error");
				return false;
			}

			return true;
		}
		#endregion
	}
}