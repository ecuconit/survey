﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json;
using System.Configuration;
using CON_Console;
using System.Data;

namespace Survey.Adapter
{
	/// <summary>
	/// Adapter for general utility
	/// </summary>
	public partial class Utility : BaseAdapter
	{
		/// <summary>
		/// Attempt to send email to survey respondents
		/// </summary>
		/// <param name="Name">Survey name</param>
		/// <param name="CC">Also send to</param>
		/// <param name="Sender">Who's sending the email</param>
		/// <param name="Subject">Email subject</param>
		/// <param name="Body">Email body</param>
		/// <returns>Error message if any, okay message if successfully</returns>
		[WebMethod]
		public static string SendMail(string Name, string CC, string Sender, string Subject, string Body)
		{
			// Get result from database
			string errorMessage = null;

			// Simple verification
			if ((string.IsNullOrWhiteSpace(Name)) || (string.IsNullOrWhiteSpace(Sender)) || (string.IsNullOrWhiteSpace(Subject)) || (string.IsNullOrWhiteSpace(Body)))
				return "Unable to send email successfully, invalid input variables";

			// Survey name verification
			string cache = GameDev.GetEmails(Name);
			if (string.IsNullOrWhiteSpace(cache))
				return "Unable to fetch survey by name.";
			List<string> emails = (List<string>)JsonConvert.DeserializeObject(cache, typeof(List<string>));
			if (emails.Count <= 0)
				return "No email associated with identified survey name.";

			// Verify email format
			emails = emails.Select(x => x.Trim()).ToList();
			if (!string.IsNullOrWhiteSpace(CC))
				emails.AddRange(CC.Split(';').Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Trim()));
			if (emails.Any(x => !DatabaseController.IsValidEmail(x)))
				return "Emails and CC contain invalid email format: " + string.Join(", ", emails.Where(x => !DatabaseController.IsValidEmail(x)));
			if (!DatabaseController.IsValidEmail(Sender))
				return "Sender email format is in an invalid format.";

			// Sending email to all
			if ((!DatabaseController.SendMail(emails, new List<string>() { Sender }, Subject, Body, ref errorMessage, Sender)) || (errorMessage != null))
				return "Unable to send emails successfully.  Error Message: " + (errorMessage ?? "Error Message");

			return okayMessage;
		}
	}
}