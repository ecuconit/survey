﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using Newtonsoft.Json;
using System.Configuration;
using CON_Console;
using System.Data;

namespace Survey.Adapter
{
	/// <summary>
	/// Adapter for the game development survey
	/// </summary>
	public partial class GameDev : BaseAdapter
	{
		#region Dictionaries
		public static List<string> majors = new List<string>() {
			"Animation and Interactive Design",
			"Art",
			"Computer Science",
			"Communication",
			"Information and Computer Technology",
			"Software Engineering",
			"Other" },
			classifications = new List<string>() {
				"Freshman", "Sophomore",
				"Junior", "Senior",
				"Trade Degree", "Certificate Degree", "Masters Degree", "Doctoral Degree", "NA" },
			sources = new List<string>() {
				"Classroom announcement",
				"Departmental email announcement",
				"Student group/club announcement",
				"Posters",
				"ECU Announce",
				"University Announce",
				"Classmates",
				"Friends",
				"Word of mouth",
				"Word of mouth - teacher",
				"Prior Event",
				"Other" },
			topics = new List<string>() {
				"[Game Mechanics Workshop]",
				"[Game Mechanics Workshops]",
				"[Leveraging Player Types]",
				"[Psychology of Gamers]",
				"[AR VR in the industry]",
				"[Content Design Strategies]",
				"[Writing for Games Workshop]",
				"[Writing for Games Workshops]",
				"[Game Career Advocacy Workshops]",
				"[Game Design Document Creation]",
				"[Game Development Methodologies]",
				"[Game Development methodologies]",
				"[Project Management]",
				"[Game Design Workshop]",
				"[Game Design Workshops]",
				"[Unity Tutorial]",
				"[Scripting Tutorial]",
				"[Modeling Tutorial]",
				"[Game Modding Workshop]",
				"[Game Modding Workshops]",
				"[Game Jam]"
			};
		// Demographic aggregate
		public static List<Tuple<string, List<int>>> majorGroup = new List<Tuple<string, List<int>>>() {
			new Tuple<string, List<int>>("Art", new List<int>() { 0, 1 } ),
			new Tuple<string, List<int>>("CSCI", new List<int>() { 2, 5 } ),
			new Tuple<string, List<int>>("Others", new List<int>() { 3, 4, 6 } )
		}, classGroup = new List<Tuple<string, List<int>>>() {
			new Tuple<string, List<int>>("Underclassmen", new List<int>() { 0, 1 } ),
			new Tuple<string, List<int>>("Upperclassmen", new List<int>() { 2, 3 } ),
			new Tuple<string, List<int>>("Graduate", new List<int>() { 6, 7 } ),
			new Tuple<string, List<int>>("Others", new List<int>() { 4, 5, 8 } )
		};
		#endregion

		#region Post Methods
		/// <summary>
		/// Save input to database
		/// </summary>
		/// <param name="input">Serialized form submit object</param>
		/// <returns>Okay message if completed successfully, error message otherwise</returns>
		/// <remarks>Example input string as followed:
		/// {
		///	surveyName: 'Survey Name',
		///	submitTime: '2020-02-02T22:22:22-05:00',
		///	browser: 'Mozilla Netscape Google Inc.',
		///	browserVersion: '5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
		///	email: 'some@email.com',
		///	major: 'Some Major',
		///	classification: 'N/A',
		///	source: 'Some sources',
		///	comment: 'Some comment',
		///	
		///	future: '[[Topic 1], [Topic 2]]',
		///	suggestion: 'Suggestion'
		/// }
		/// </remarks>
		[WebMethod]
		public static string Post(string input)
		{
			// Deseralize input values
			dynamic form = JsonConvert.DeserializeObject(input);
			string errorMessage = null;

			try
			{
				// Saving to DB
				Dictionary<string, object> param = new Dictionary<string, object>();
				param.Add("@surveyName", (string)form["surveyName"] ?? ConfigurationManager.AppSettings["defaultName"]);
				param.Add("@processTime", DateTime.Now);
				param.Add("@submitTime", (string)form["submitTime"]);
				param.Add("@browser", (string)form["browser"]);
				param.Add("@browserVersion", (string)form["browserVersion"]);

				param.Add("@email", ((string)form["email"]).Trim());
				param.Add("@major", (string)form["major"]);
				param.Add("@classification", (string)form["classification"]);
				param.Add("@source", (string)form["source"]);
				param.Add("@comment", (string)form["comment"]);

				param.Add("@future", (string)form["future"]);
				param.Add("@suggestion", (string)form["suggestion"]);

				param.Add("@raw", (string)input);

				// Save to DB
				int reply = (int)DatabaseController.InsertUpdateReturn("InsertGameDevSurvey", param, ref errorMessage);
				if ((reply <= 0) || (errorMessage != null))
					return "Unable to save form successfly.  Please contact system administrator chenk@ecu.edu.  Error Message: " + (errorMessage ?? "Unknown Error");
			}
			catch (Exception error)
			{
				return "Caught exception.  Please contact system administrator chenk@ecu.edu.  Error Message: " + error.Message + ". Stack: " + error.StackTrace;
			}

			return okayMessage;
		}
		#endregion

		#region Get Methods
		/// <summary>
		/// Get the demographic result of the survey
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		[WebMethod]
		public static string GetDemographics(string input)
		{
			// Get result from database
			string errorMessage = null;
			Dictionary<string, object> param = new Dictionary<string, object>();
			param.Add("@surveyName", input);
			DataTable result = DatabaseController.SelectReturn("SelectGameDevDemographics", param, ref errorMessage);

			// Simple verification
			if ((result == null) || (result.Rows.Count < 1) || (errorMessage != null))
				return "Unable to get results successfully.  Error Message:  " + (errorMessage ?? "Unknown Error");

			// Parse and return result
			return JsonConvert.SerializeObject(
				result.AsEnumerable().Select(x => new
				{
					Count = x["Count"],
					StartTime = x["StartTime"],
					EndTime = x["EndTime"],
					Average = x["AverageTopics"]
				}));
		}

		/// <summary>
		/// Get the analytical result of the survey
		/// </summary>
		/// <param name="input">Survey name</param>
		/// <returns>Serialized partial result of the survey</returns>
		[WebMethod]
		public static string GetAnalytics(string input)
		{
			// Get result from database
			string errorMessage = null;
			Dictionary<string, object> param = new Dictionary<string, object>();
			param.Add("@surveyName", input);
			DataTable result = DatabaseController.SelectReturn("SelectGameDevFuture", param, ref errorMessage);

			// Simple verification
			if ((result == null) || (result.Rows.Count < 1) || (errorMessage != null))
				return "Unable to get results successfully.  Error Message:  " + (errorMessage ?? "Unknown Error");

			// Parse dictionary
			List<string> tempTopics;
			List<int> topicList;
			int[] tempParsed = new int[3];
			// Key: major ID, major agg ID, class ID, class agg ID, source ID, topics list
			List<Tuple<int, int, int, int, int, List<int>>> parsed = new List<Tuple<int, int, int, int, int, List<int>>>();
			result.AsEnumerable().ToList<DataRow>().ForEach(x =>
			{
				// Topic parsing
				tempTopics = new List<string>();
				topicList = new List<int>();
				if (!string.IsNullOrWhiteSpace((string)x["Suggestion"]))
				{
					tempTopics = ((string)x["Suggestion"]).Split(',').Select(y => y.Trim()).ToList();
					tempTopics.ForEach(y =>
					{
						if (topics.IndexOf(y) < 0)
							topics.Add(y);
					});
				}
				if (!string.IsNullOrWhiteSpace((string)x["Future"]))
					tempTopics.AddRange(((string)x["Future"]).Split(',').Select(y => y.Trim()));
				topicList = tempTopics.Where(y => !string.IsNullOrWhiteSpace(y)).ToList().Select(y => topics.IndexOf((string)y)).ToList();
				topicList.Sort();

				// Flags parsing
				tempParsed[0] = majors.IndexOf((string)x["Major"]);
				if (tempParsed[0] < 0) tempParsed[0] = majors.Count - 1;
				tempParsed[1] = classifications.IndexOf((string)x["Classification"]);
				if (tempParsed[1] < 0) tempParsed[1] = majors.Count - 1;
				tempParsed[2] = sources.IndexOf((string)x["Source"]);
				if (tempParsed[2] < 0) tempParsed[2] = majors.Count - 1;
				parsed.Add(new Tuple<int, int, int, int, int, List<int>>(
					tempParsed[0], MajorAggregation(tempParsed[0]),
					tempParsed[1], ClassAggregation(tempParsed[1]),
					tempParsed[2], topicList));
			});

			// Initialize topics aggregation
			// Key: topic ID, major count, major agg count, class count, class agg count, source count, [total, weighted total]
			List<Tuple<int, int[], int[], int[], int[], int[], float[]>> topicsCount = Enumerable.Range(0, topics.Count).Select(x =>
				new Tuple<int, int[], int[], int[], int[], int[], float[]>(
					x, new int[majors.Count], new int[majorGroup.Count],
					new int[classifications.Count], new int[classGroup.Count],
					new int[topics.Count], new float[] { 0.0f, 0.0f }
				)
			).ToList();

			// Topics counting
			float weight = 0.0f;
			parsed.ForEach(x =>
			{
				// Calculate weighted topic
				weight = 1.0f / (float)x.Item6.Count;

				// Loop through topics
				x.Item6.ForEach(y =>
				{
					// Add to cache
					topicsCount[y].Item2[x.Item1]++;
					topicsCount[y].Item3[x.Item2]++;
					topicsCount[y].Item4[x.Item3]++;
					topicsCount[y].Item5[x.Item4]++;
					topicsCount[y].Item6[x.Item5]++;
					topicsCount[y].Item7[0]++;
					topicsCount[y].Item7[1] += weight;
				});
			});

			// Parse and return result
			return JsonConvert.SerializeObject(new
			{
				// Dictionary = new { majors, classifications, sources, topics },
				// Topics = parsed,
				// Raw = result,
				// Count = topicsCount,
				Charts = new
				{
					Item1 = new
					{
						ID = "diagram1_1",
						Title = "Topics Summary",
						Type = "Column",
						XAxis = topics,
						XAxisDesc = "Future Topics",
						//YAxis = topics,
						YAxisDesc = "Count",
						Data = topicsCount.Select(x => new object[] { topics[x.Item1], (int)x.Item7[0] }).ToList(),
						Desc = "Total sum of selected topics.  Suggested topic(s) are marked in double brackets."
					},
					Item2 = new
					{
						ID = "diagram1_2",
						Title = "Weighted Topics",
						Type = "Column",
						XAxis = topics,
						XAxisDesc = "Future Topics",
						//YAxis = topics,
						YAxisDesc = "Average Sum",
						Data = topicsCount.Select(x => new object[] { topics[x.Item1], (float)x.Item7[1] }).ToList(),
						Desc = "Weighted total sum of selected topics by number of option(s) selected per entry.  Suggested topic(s) are marked in double brackets."
					},
					Item3 = new
					{
						ID = "diagram2_1",
						Title = "Majors Breakdown",
						Type = "Pie",
						XAxis = majors,
						XAxisDesc = "Majors",
						//YAxis = topics,
						YAxisDesc = "Average Sum",
						Data = parsed.GroupBy(x => x.Item1).Select(x => new { name = majors[x.Key], y = x.Count() }).ToList(),
						Desc = "Percentage distribution of current or intended major of the survey respondents."
					},
					Item4 = new
					{
						ID = "diagram2_2",
						Title = "Classification Breakdown",
						Type = "Pie",
						XAxis = classifications,
						XAxisDesc = "Classification",
						//YAxis = topics,
						YAxisDesc = "Average Sum",
						Data = parsed.GroupBy(x => x.Item3).Select(x => new { name = classifications[x.Key], y = x.Count() }).ToList(),
						Desc = "Percentage distribution of classification of the survey respondents."
					},
					Item5 = new
					{
						ID = "diagram2_3",
						Title = "Sources of Information",
						Type = "Pie",
						XAxis = sources,
						XAxisDesc = "Sources",
						//YAxis = topics,
						YAxisDesc = "Average Sum",
						Data = parsed.GroupBy(x => x.Item5).Select(x => new { name = sources[x.Key], y = x.Count() }).ToList(),
						Desc = "Percentage distribution of where people have heard of this event from."
					},
					Item6 = new
					{
						ID = "diagram3",
						Title = "Demographic Breakdown",
						Type = "Heat",
						XAxis = classifications,
						XAxisDesc = "Student classification",
						YAxis = majors,
						YAxisDesc = "Student's major selection",
						Data = parsed.GroupBy(x => new { x.Item1, x.Item3 }).Select(x => new int[] { x.Key.Item3, x.Key.Item1, x.Count() }).ToList(),
						Desc = "Demographic information broken down by student classification crossing with self identified major in the form of a heatmap."
					},
					Item7 = new
					{
						ID = "diagram4",
						Title = "Demographic Breakdown",
						Type = "Heat",
						XAxis = classifications,
						XAxisDesc = "Student classification",
						YAxis = sources,
						YAxisDesc = "Information sources",
						Data = parsed.GroupBy(x => new { x.Item5, x.Item3 }).Select(x => new int[] { x.Key.Item3, x.Key.Item5, x.Count() }).ToList(),
						Desc = "Demographic information broken down by student classification crossing with where they have heard of this event."
					},
					Item8 = new
					{
						ID = "diagram5",
						Title = "Topics Breakdown by Aggregated Major",
						Type = "StackColumn",
						XAxis = topics,
						XAxisDesc = "Future Topics",
						YAxis = majorGroup.Select(x => x.Item1).ToList(),
						YAxisDesc = "Major Groups",
						Data = Enumerable.Range(0, majorGroup.Count()).Select(x => new { name = majorGroup[x].Item1, data = topicsCount.Select(y => y.Item3[x]).ToArray() }).ToList(),
						Desc = "Topics selection broken down by respondents' aggregated major groupings such as Art, Computer Science, and others."
					},
					Item9 = new
					{
						ID = "diagram6",
						Title = "Topic Breakdown by Aggregated Classification",
						Type = "StackColumn",
						XAxis = topics,
						XAxisDesc = "Future Topics",
						YAxis = classGroup.Select(x => x.Item1).ToList(),
						Data = Enumerable.Range(0, classGroup.Count()).Select(x => new { name = classGroup[x].Item1, data = topicsCount.Select(y => y.Item5[x]).ToArray() }).ToList(),
						Desc = "Topics selection broken down by respondents' aggregated classification groupings such as underclassmen, upperclassmen, graduate, and others."
					}
				}
			});
		}

		/// <summary>
		/// Get the feedback result of the survey
		/// </summary>
		/// <param name="input">Survey name</param>
		/// <returns>Serialized partial result of the survey</returns>
		[WebMethod]
		public static string GetFeedbacks(string input)
		{
			// Get result from database
			string errorMessage = null;
			Dictionary<string, object> param = new Dictionary<string, object>();
			param.Add("@surveyName", input);
			DataTable result = DatabaseController.SelectReturn("SelectGameDevComments", param, ref errorMessage);

			// Simple verification
			if ((result == null) || (result.Rows.Count < 1) || (errorMessage != null))
				return "Unable to get results successfully.  Error Message:  " + (errorMessage ?? "Unknown Error");

			// Comment parsing
			List<string> comments = new List<string>(), words = new List<string>();
			result.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace((string)x["Comment"])).ToList().ForEach(x =>
			{
				// Buffer comments
				if ((x["Comment"] != DBNull.Value) && (!string.IsNullOrWhiteSpace((string)x["Comment"])))
					comments.Add((string)x["Comment"]);
				// Parse words
				if ((x["Words"] != DBNull.Value) && (!string.IsNullOrWhiteSpace((string)x["Words"])))
					words.AddRange(((string)x["Words"]).Split(';').Select(y => y.Trim()));
			});

			return JsonConvert.SerializeObject(new
			{
				Comments = comments,
				Words = words
			});
		}

		/// <summary>
		/// Get the email list result of the survey
		/// </summary>
		/// <param name="input">Survey name</param>
		/// <returns>Serialized partial result of the survey</returns>
		[WebMethod]
		public static string GetEmails(string input)
		{
			// Get result from database
			string errorMessage = null;
			Dictionary<string, object> param = new Dictionary<string, object>();
			param.Add("@surveyName", input);
			DataTable result = DatabaseController.SelectReturn("SelectGameDevContacts", param, ref errorMessage);

			// Simple verification
			if ((result == null) || (result.Rows.Count < 1) || (errorMessage != null))
				return "Unable to get results successfully.  Error Message:  " + (errorMessage ?? "Unknown Error");

			return JsonConvert.SerializeObject(result.AsEnumerable().Where(x => !string.IsNullOrWhiteSpace((string)x["Email"])).Select(x => (string)x["Email"]).Distinct());
		}
		#endregion

		#region Helper Methods
		/// <summary>
		/// Simple echo method
		/// </summary>
		/// <param name="input">Input value from user</param>
		/// <returns>Exact return</returns>
		[WebMethod]
		public static string Echo(string input)
		{
			return input;
		}

		/// <summary>
		/// Aggregates classification into manageable groups
		/// </summary>
		/// <param name="index">Original index</param>
		/// <returns>Condensed index</returns>
		private static int ClassAggregation(int index)
		{
			// Loop search
			for (int i = 0; i < classGroup.Count; i++)
			{
				if (classGroup[i].Item2.IndexOf(index) >= 0)
					return i;
			}
			return classGroup.Count - 1;
		}

		/// <summary>
		/// Aggregates major into manageable groups
		/// </summary>
		/// <param name="index">Original index</param>
		/// <returns>Condensed index</returns>
		private static int MajorAggregation(int index)
		{
			// Loop search
			for (int i = 0; i < majorGroup.Count; i++)
			{
				if (majorGroup[i].Item2.IndexOf(index) >= 0)
					return i;
			}
			return majorGroup.Count - 1;
		}
		#endregion
	}
}